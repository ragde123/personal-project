//
//  SecondView.swift
//  FirstApp
//
//  Created by Mac Mini on 10/22/19.
//  Copyright © 2019 Mac Mini. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class SecondView: UIViewController{
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = false
    }
}
